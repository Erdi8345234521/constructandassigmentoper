// OperatorOverloading.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>

using namespace std;

//~~~Begin FVector
struct FVector
{
public:
	FVector()
	{
		this->x = 0.f;
		this->y = 0.f;
		this->z = 0.f;
	}

	FVector(const float x, const float y, const float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	explicit FVector(float Value)
	{
		this->x = Value;
		this->y = Value;
		this->z = Value;
	}

	FVector(int x) = delete;

	FVector(const FVector& Other)
	{
		this->x = Other.x;
		this->y = Other.y;
		this->z = Other.z;
	}
	
	explicit operator float() const
	{
		return sqrt(x * x + y * y + z * z);
	}
	
	friend FVector operator+(const FVector& a, const FVector& b);

	friend FVector operator-(const FVector& a, const FVector& b);
	
	friend FVector operator*(const FVector& a, float b);
		
	friend FVector operator*(const FVector& a, const FVector& b);
	
	friend ostream& operator<<(ostream& out, const FVector& a);

	friend istream& operator>>(istream& in, FVector& a);
	
	friend bool operator>(const FVector& a, const FVector& b);

	float operator[](int index) const
	{
		switch (index)
		{
		case 1:
			return x;
		break;
			
		case 2:
			return y;
		break;

		case 3:
			return z;
		break;

		default:
			cout << "Invalid index";
			return 0;
		}	
	}
	
private:
	float x;
	float y;
	float z;
};

FVector operator+(const FVector& a, const FVector& b)
{
	return FVector(a.x + b.x, a.y + b.y, a.z + b.z);
}

FVector operator-(const FVector& a, const FVector& b)
{
	return FVector(a.x - b.x, a.y - b.y, a.z - b.z);
}

FVector operator*(const FVector& a, float b)
{
	return FVector(a.x * b, a.y * b, a.z * b);
}

FVector operator*(const FVector& a, const FVector& b)
{
	return FVector(a.x * b.x, a.y * b.y, a.z * b.z);
}

ostream& operator<<(ostream& out, const FVector& a)
{
	out << a.x << ' ' << a.y << ' ' << a.z;
	return out;
}

istream& operator>>(istream& in, FVector& a)
{
	in >> a.x >> a.y >> a.z;
	return in;
}

bool operator>(const FVector& a, const FVector& b)
{
	return false;
}
//~~~End FVector

class MyClass
{
public:
	MyClass()
	{
		Arr = nullptr;
		
		//Init Dynamic String
		Str1 = new string("Null");

		//Init Dynamic FVector
		v1 = new FVector(0.f);
	}

	/*Constructor For Dynamic Data */
	MyClass(int Rows, int Cols, string Str, const FVector& vector)
	{
		RowsArr = Rows;
		ColsArr = Cols;
		
		//Init Dynamic Array
		Arr = new int* [RowsArr];
		for (int x = 0; x < RowsArr; x++)
		{
			Arr[x] = new int[ColsArr];
			for(int y = 0; y < ColsArr; y++)
			{
				Arr[x][y] = rand() % 20;
			}
		}
		
		//Init Dynamic String
		Str1 = new string(Str);

		//Init Dynamic FVector
		v1 = new FVector(vector);
	}

	/** Destructor */
	~MyClass()
	{
		for(int x = 0; x < RowsArr; x++)
		{
			delete[] Arr[x];
		}
		delete Arr;
		delete Str1;
		delete v1;		
	};

	/** Copy constructor */
	MyClass(const MyClass& other)
	{
		ColsArr = other.ColsArr;
		RowsArr = other.RowsArr;
		Str1 = new string(*(other.Str1));
		v1 = new FVector(*(other.v1));
		Arr = new int* [RowsArr];
		for (int x = 0; x < ColsArr; x++)
		{
			Arr[x] = new int[ColsArr];
			for (int y = 0; y < ColsArr; y++)
			{
				Arr[x][y] = other.Arr[x][y];
			}
		}
	}

	/** Assignment operator */
	MyClass& operator=(MyClass& other)
	{
		ColsArr = other.ColsArr;
		RowsArr = other.RowsArr;

		delete Str1;
		Str1 = new string(*(other.Str1));

		delete v1;
		v1 = new FVector(*(other.v1));
	
		Arr = new int* [RowsArr];
		for(int x = 0; x < ColsArr; x++)
		{
			Arr[x] = new int [ColsArr];
			for (int y = 0; y < ColsArr; y++)
			{
				Arr[x][y] = other.Arr[x][y];
			}
		}

		return (*this);
	}

	/** Print data from Dynamic Array */
	void PrintArray() const
	{
		for(int x = 0; x < RowsArr;x++)
		{
			for(int y = 0; y < ColsArr; y++)
			{
				cout << "Array: " << Arr[x][y] << '\t';
			}
			cout << endl;
		}
	}

	/** Print  data from Dynamic FVector struct */
	void PrintVector() const
	{
		cout << "\nVector: " << *v1;
	}

	/** Print data from Dynamic string */
	void PrintString() const
	{
		cout << "\nString: " << *Str1;
	}
	
private:
	int ColsArr = 1;
	int RowsArr = 6;
	
	string* Str1;
	FVector* v1;
	int** Arr;
};

int main()
{
	MyClass my_class1(2, 2, "Hello, World!", FVector(2.f));
	MyClass my_class2;

	my_class2 = my_class1;

	my_class2.PrintArray();
	my_class2.PrintVector();
	my_class2.PrintString();
}
